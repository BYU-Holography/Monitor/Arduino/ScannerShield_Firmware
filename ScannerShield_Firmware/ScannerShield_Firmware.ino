/*
BYU Holographic Video Monitor Scanner Shield Runtime Firmware

Copyright 2016-2019 BYU ElectroHolography Research Group

Email: byuholography@gmail.com

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma GCC push_options
#pragma GCC optimize( "O3" ) // helps speed up a lot of operations

#include <SPI.h>
#include <Wire.h> // I2C

#include "ScanShield_Firmware_Setup.h"
#include "ScanShield_Firmware_PLL_Filter.h"
#include "ScanShield_Firmware_Control.h"

#include "ScanShield_Firmware_Sampling.h"
// ..._Sampling.h must be the last header, as it contains pointers to a bunch of other variables declared in the previous header files





/////////////////////////////////////
//
// SETUP ENTRY POINT
//
/////////////////////////////////////

void setup()
{
  // initialize board GPIO
  init_Scanner_Shield();

  // Serial setup
  Serial.begin( 9800 );
  Serial.println( "Setting baud rate to 500,000..." );
  Serial.flush();
  
  // 250000 baud needed for verbose data sampling
  // 10,000 line buffer (80 chars wide) is enough for 160,000 samples, est. 34 seconds under normal lock conditions
  //Serial.begin( 250000 );
  Serial.begin( 500000 );
  //Serial.enable_interrupts_in_UDRE_ISR();
  
  // effective limits on Arduino maximum baud rate from unoptimized Arduino implementation:
  // https://arduino.stackexchange.com/questions/296/how-high-of-a-baud-rate-can-i-go-without-errors
  
  // Serial TX blocks when trying to write to full TX buffer:
  // https://forum.arduino.cc/index.php?topic=360286.0

  // clear Serial buffer
  Serial.flush();

  // indicate program has started
  // useful to tell where the program hangs, if it does
  
  // C preprocessor macros:
  // https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
  
  Serial.println();
  Serial.println( "Alive. Firmware compiled/uploaded on " __DATE__ " at " __TIME__ "." );
  Serial.println();
  
  ringBells( 2 );
  // triggers system bell sound in the serial terminal TeraTerm
  
  // buzzer, LED "hello"
  for( int i = 550; i >= 0; i-- )
  {
    digitalWrite( PIN_BUZZER, i & 1 );
    digitalWrite( PIN_LED_ACT_SPI, i>>7 & 1 );
    digitalWrite( PIN_LED_ACT_I2C, i>>4 & 1 );
    digitalWrite( PIN_LED_HSYNC, i>>5 & 1 );
    digitalWrite( PIN_LED_VSYNC, i>>6 & 1 );
    delay(i>>8);
  }
  
  digitalWrite( PIN_BUZZER, LOW );
  digitalWrite( PIN_LED_ACT_SPI, LOW );
  digitalWrite( PIN_LED_ACT_I2C, LOW );
  digitalWrite( PIN_LED_HSYNC, LOW);
  digitalWrite( PIN_LED_VSYNC, LOW );
  
  // print out list of characters for development, if needed
  //printCharacterMapping();
  
  // evaluate the behavior and performance of various print functions and techniques
  //test_serial();
  
  // reprogram EDID, if needed
  //Run_EDID_Setup();

  // wait for everything to print before getting to main code loop
  Serial.flush();
  
  // enable interrupts during UDRE ISR to improve DSYNC & PTACH ISR clock measurement accuracy
  // this shouldn't be needed with Input Capture Compare units
  //Serial.enable_interrupts_in_UDRE_ISR();
}





/////////////////////////////////////
//
// RUNTIME LOOP
//
/////////////////////////////////////

void loop()
{
  // measure the amount of time it takes to perform computations

  loop_clock_start_last = loop_clock_start;
  
  noInterrupts();
  loop_clock_start = TCNT3;
  interrupts();
  
  //loop_total_cycles = loop_clock_start - loop_clock_start_last;
  //int loop_print_cycles = loop_clock_start - loop_clock_stop;
  
  
  
  // subtract from the amount of time left in this DSYNC period
  
  //DSYNC_period_cycles_remaining -= loop_total_cycles;
  
  
  
  // wait three seconds for motor to spin up before we can start trusting PTACH
  
  if( spinup_wait && loop_clock_start_last > loop_clock_start )
  {
    loop_clock_overflow_counter++;
    
    if( loop_clock_overflow_counter > 732 )
    {
      spinup_wait = false;
      loop_clock_overflow_counter = 0;
    }
  }
  
  

  if( VSYNC_event_occurred )
  {
    VSYNC_event_occurred = false;

    // reset the galvo position
    
    galvo_value = 0;
    dac_write( DAC_CH_GALVO, galvo_value );
    
  } // end if( VSYNC_event_occurred )
  
  
  
  // handle DSYNC (divided HSYNC) events

  if( DSYNC_event_occurred )
  {
    DSYNC_event_occurred = false;
    
    // DSYNC (INT4) has precedence over PTACH (INT5)
    // there should never be a repeat DSYNC event within this function
    
    // start measuring how many clock cycles this function takes
    
    noInterrupts();
    unsigned int temp_clock_start = TCNT3;
    interrupts();
    
    
    
    DSYNC_update_actions();
    
    
    
    // record the number of clock cycles this function took to complete
    
    noInterrupts();
    unsigned int temp_clock_stop = TCNT3;
    interrupts();
    
    loop_computation_cycles_accumulator += temp_clock_stop - temp_clock_start;
    
    // trigger secondary event (controller update)
    
    DSYNC_event_secondary = true;
    
  }
  
  
  
  // handle PTACH (polygon tachometer) events

  if( PTACH_event_occurred )
  {
    PTACH_event_occurred = false;
    
    // DSYNC (INT4) has precedence over PTACH (INT5)
    // there should never be a repeat PTACH event within this function

    // start measuring how many clock cycles this function takes
    
    noInterrupts();
    unsigned int temp_clock_start = TCNT3;
    interrupts();
    
    
    
    PTACH_update_actions();
    
    
    
    // record the number of clock cycles this function took to complete
    
    noInterrupts();
    unsigned int temp_clock_stop = TCNT3;
    interrupts();
    
    loop_computation_cycles_accumulator += temp_clock_stop - temp_clock_start;
  }
  
  
  
  // perform phase filter and phase delta computations
  
  if( PHASE_updated )
  {
    PHASE_updated = false;
    
    // all of this could be done in the PTACH_event_occurred handler, but it's nicer
    // to separate these large phase-specific code chunks into a separate context
    
    // start measuring how many clock cycles this function takes
    
    noInterrupts();
    unsigned int temp_clock_start = TCNT3;
    interrupts();
    
    
    
    PHASE_update_actions();
    
    
    
    // record the number of clock cycles this function took to complete
    
    noInterrupts();
    unsigned int temp_clock_stop = TCNT3;
    interrupts();
    
    loop_computation_cycles_accumulator += temp_clock_stop - temp_clock_start;
  }
  
  
  
  // update the control algorithm state if enabled
  
  if( enable_period_control && DSYNC_event_secondary )
  {
    DSYNC_event_secondary = false;
    
    // start measuring how many clock cycles this function takes
    
    noInterrupts();
    unsigned int temp_clock_start = TCNT3;
    interrupts();
    
    
    
    PLL_controller_update();
    
    
    
    // record the number of clock cycles this function took to complete
    
    noInterrupts();
    unsigned int temp_clock_stop = TCNT3;
    interrupts();
    
    loop_computation_cycles_accumulator += temp_clock_stop - temp_clock_start;
  }
  
  
  
  // LABEL: MOTOR_VALUE UPDATE
  // update the commanded value of the polygon motor voltage / speed if a command change has occurred
  // this lets variable changes trigger SPI writes only when needed
  // don't clip motor_value so that we can see what it actually is in the telemetry
  
  if( motor_value != motor_value_prev )
  {
    motor_value_prev = motor_value;

    int motor_command = motor_value;
    
    if( motor_command > 4095 )
      motor_command = 4095;
    if( motor_command < 0 )
      motor_command = 0;
    
    dac_write( DAC_CH_POLY, motor_command );
  }
  
  
  
  // End of Computations
  // general printing goes after here to ensure measurement accuracy of computational workload
  
  
  
  //noInterrupts();
  //loop_clock_stop = TCNT3;
  //interrupts();
  
  //loop_computation_cycles = loop_clock_stop - loop_clock_start;
  
  
  
  // print variables of interest
  
  // TODO: be able to adjust print_vars[ ... ].enabled with keyboard
  
  if( print_enabled )
  {
    // start measuring how many clock cycles this function takes
    // NOTE: this time-measuring technique doesn't work exactly as intended...
    // there's a while loop that checks all the variables that's always executing, regardless of whether a variable is printed or not
    // so even if there's no variable to print, this incorrect way of measuring clock cycles will measure the "overhead" repeatedly
    // thanks to the loop() loop
    
    //noInterrupts();
    //unsigned int temp_clock_start = TCNT3;
    //interrupts();
    
    
    
    send_telemetry();
    
    
    
    // record the number of clock cycles this function took to complete
    
    //noInterrupts();
    //unsigned int temp_clock_stop = TCNT3;
    //interrupts();
    
    //loop_print_cycles_accumulator += temp_clock_stop - temp_clock_start;
  }
  
} // end void loop()





/**
   Writes a value to the DAC using the SPI protocol and arduino pins
   @param address which of two onchip DACs you are writing the value to (0 or 1)
   @param value the value to be written to the DAC, between 0-1023 inclusive
*/
void dac_write( int address, int value )
{
  //see http://www.datasheetarchive.com/dl/Datasheet-091/DSA0036741.pdf
  // on page 22 for packet details for MCP4822 12-bit DAC
  // takes 16-bit data packets (2 bytes)

  //  send in the address and value via SPI:
  value &= 4095; // rollover values that require more than 12 bits
  value |= ( address << 15 ); //place address is specified part
  value |= ( 1 << 12 ); // enable output
  // take the SS pin low to select the chip:
  digitalWrite( PIN_DAC_SPI_CS, LOW );
  // gain set to 2x by default (bit 13 == 0)
  SPI.transfer( highByte( value ) );
  SPI.transfer( lowByte( value ) );
  // take the SS pin high to de-select the chip:
  digitalWrite( PIN_DAC_SPI_CS, HIGH );
}




void print_temperature_readable( int temp_val )
{
  // for the TMP36 temperature sensor, Voff: 0.5V, scaling: 10 mV/C, Vout(25C)=750
  // Arduino uses 10-bit values: 0-1023 for 0-5V without changing analogReference() - 4.9 mV/step
  
  // ( analogRead(PIN_TEMP_SENSOR) / 1023 * 5 - 0.5V )/ 0.010V/C = temp in C
  // temp_val * 0.48875 - 50
  
  float temp_report = temp_val * 0.48875855f - 50.0f;
  
  Serial.print( temp_report, 2 );
  Serial.println( 'C' );
}




void serialEvent()
{
  while( Serial.available() )
  {
    // get the new byte:
    char inChar = (char) Serial.read();

    switch( inChar )
    {
      case 'c':
        // enable / disable PLL control algorithm
      
        //print_enabled = !print_enabled;
        enable_period_control = !enable_period_control;
        PIN_POLY_ENABLE_state = !enable_period_control;
        digitalWrite( PIN_POLY_ENABLE, PIN_POLY_ENABLE_state );
        if( enable_period_control )
        {
          spinup_wait = true;
          loop_clock_overflow_counter = 0;
          ki_period_term_long_sum = 0L;
          ki_phase_term_long_sum = 0L;
        }
        Serial.print( "\nPeriod control " );
        Serial.println( enable_period_control ? "ENABLED" : "disabled" );
        break;
        
      case 'e':
        // enable / disable phase filtering
      
        filter_PHASE = !filter_PHASE;
        Serial.print( "\nphase filtering " );
        Serial.println( filter_PHASE ? "ON" : "off" );
        break;
        
      case 't':
        // print the polygon motor transistor approximate temperature
      
        print_temperature_readable( analogRead(PIN_TEMP_SENSOR) );
        break;

      case 'h':
        // toggle the Hot Plug pin state in the DVI connection
        
        PIN_HOT_PLUG_state = !PIN_HOT_PLUG_state;
        digitalWrite( PIN_HOT_PLUG, PIN_HOT_PLUG_state );
        Serial.println( "hot_plug = " + String( PIN_HOT_PLUG_state ) );
        break;
        
      case 'i':
        // manually invert the DSYNC divider
        
        noInterrupts();
        DSYNC_divide_by_two_flag = !DSYNC_divide_by_two_flag;
        interrupts();
        Serial.println( "inverted DSYNC software divider" );
        break;

      case 'm':
        // enable / disable the motor
      
        PIN_POLY_ENABLE_state = !PIN_POLY_ENABLE_state;
        digitalWrite( PIN_POLY_ENABLE, PIN_POLY_ENABLE_state );

        Serial.println( "motor_enable = " + String( PIN_POLY_ENABLE_state ) );
          
        break;
        
      case 'p':
        // enable / disable printing
      
        print_enabled = !print_enabled;
        break;

      case '4':
      case '6':
      case '2':
      case '8':
        // manually adjust motor speed
      
        if( !enable_period_control )
        {
          switch( inChar )
          {
            case '4':
              motor_value--;
              break;

            case '6':
              motor_value++;
              break;

            case '2':
              motor_value -= 10;
              break;

            case '8':
              motor_value += 10;
              break;
          }
          
          if ( motor_value < 0 )
            motor_value = 0;
          if ( motor_value > 4095 )
            motor_value = 4095;
          
          Serial.println( "motor_value = " + String( motor_value ) );
          
          dac_write(DAC_CH_POLY, motor_value);
        }
        
        break;
        
    } // end switch( inChar )
    
  } // end while (Serial.available())
}



















