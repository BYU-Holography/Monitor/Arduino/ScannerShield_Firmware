/////////////////////////////////////
//
// SETUP & SETUP HELPER FUNCTIONS
//
/////////////////////////////////////



void ringBells( unsigned char times )
{
  int i;
  for( i = 0; i < times; i++ )
  {
    Serial.print( (char) 7 ); // BEL signal
    delay( 100 );
  }
}



void init_Scanner_Shield()
{
  // GPIO is by default configured as inputs; but we're just being explicit here

  // interruptable pins
  pinMode( PIN_DSYNC, INPUT );
  pinMode( PIN_PTACH, INPUT );
  pinMode( PIN_HSYNC, INPUT );
  pinMode( PIN_VSYNC, INPUT );

  // DVI breakout
  pinMode( PIN_HOT_PLUG,  OUTPUT );
  digitalWrite( PIN_HOT_PLUG, PIN_HOT_PLUG_state );
  pinMode( PIN_MIXAMP0, INPUT );
  pinMode( PIN_MIXAMP1, INPUT );
  pinMode( PIN_MIXAMP2, INPUT );

  // configure hardware divider via PORTC
  DDRC  = 0xFF; // set all as outputs
  PORTC = 0x1F; // 0x1F for divide-by-32; 0x3F for divide-by-64
                // PLL works when f_DSYNC = 2 * f_Poly = f_HSYNC / N_CRHL

  // PWM & Polygon Control
  pinMode( PIN_ESC_PWM, INPUT ); // keep as input until needed

  //configure the SPI bus
  SPI.begin();
  SPI.setDataMode( SPI_MODE0 );
  SPI.setBitOrder( MSBFIRST );
  pinMode( PIN_DAC_SPI_CS, OUTPUT );
  digitalWrite( PIN_DAC_SPI_CS, HIGH );

  // Galvo initialization
  dac_write( DAC_CH_GALVO, galvo_value );

  // Polygon Initialization
  pinMode( PIN_POLY_ENABLE, OUTPUT );
  digitalWrite( PIN_POLY_ENABLE, PIN_POLY_ENABLE_state );
  pinMode( PIN_POLY_LOCKED, INPUT );
  dac_write( DAC_CH_POLY, motor_value );
  
  // Auxiliary Circuits for Tue 26 Sep 2017 board revision
  pinMode( PIN_BUZZER, OUTPUT );
  pinMode( PIN_TEMP_SENSOR, INPUT ); // no setup needed for analog inputs
  pinMode( PIN_LED_ACT_SPI, OUTPUT );
  pinMode( PIN_LED_ACT_I2C, OUTPUT );
  pinMode( PIN_LED_HSYNC, OUTPUT );
  pinMode( PIN_LED_VSYNC, OUTPUT );

  // I2C for EDID
  Wire.begin(); // join i2c bus (address optional for master)


  // Arduino's code for interrupts is too slow, so use the registers directly
  // see https://billgrundmann.wordpress.com/2009/03/02/the-overhead-of-arduino-interrupts/

  //attachInterrupt(digitalPinToInterrupt(PIN_DSYNC), ISR_DSYNC, RISING); // INT4
  //attachInterrupt(digitalPinToInterrupt(PIN_PTACH), ISR_PTACH, CHANGING); // INT5
  //attachInterrupt(digitalPinToInterrupt(PIN_HSYNC), ISR_HSYNC, RISING); // INT3
  //attachInterrupt(digitalPinToInterrupt(PIN_VSYNC), ISR_VSYNC, RISING); // INT2

  

  // http://gammon.com.au/interrupts
  // https://www.arduino.cc/en/Reference/AttachInterrupt
  // https://www.arduino.cc/en/Reference/DetachInterrupt
  // https://www.arduino.cc/en/Reference/NoInterrupts
  // page 101 of Atmel 2560 datasheet:      http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-2549-8-bit-AVR-Microcontroller-ATmega640-1280-1281-2560-2561_datasheet.pdf
  //   old datasheet link before Atmel was bought by Microchip: http://www.atmel.com/Images/Atmel-2549-8-bit-AVR-Microcontroller-ATmega640-1280-1281-2560-2561_datasheet.pdf

  
  
  // BEGIN for Input Capture Units

  /*

    each 16-bit counter has an Input Capture Unit, which will automatically save a counter value upon an edge-triggered event
    Timer/Counters 1, 3, 4, 5 are 16-bit

    // Pin 47 - new SPI DAC CS (for Rev. 26 Sep 2017 Scanner Shield)
    // Pin 48 / PL1 - DSYNC        - ICP5
    // Pin 49 / PL0 - PTACH, PHASE - ICP4; old SPI DAC CS (for Rev. 1 Aug 2016 Scanner Shield)
    // Pin NA / PE7 - general      - ICP3
    // Pin NA / PD4 -              - ICP1
    
  */

  // §
  // TCNTnH, TCNTnL = TCNTn 
  TCNT3 = 0x0000;
  TCNT4 = 0x0000;
  TCNT5 = 0x0001;
  // set the timer/counter initial value BEFORE enabling the clock so that they appear to have both been started simultaneously in runtime
  // statistical analysis shows that the DSYNC clock, TCNT5, should be initialized 1 clock cycle ahead of the PTACH clock, TCNT4
  //   in order to center the locked phase distribution on 0
  // move this clock forward one clock cycle (how long it takes to perform "TCCR5B = 0xC1;" with -O3 optimization) so that TCNT4 & TCNT5 are the same running together

  // §
  TCCR3A = 0x00;
  TCCR4A = 0x00; // 00000000 // default value
  TCCR5A = 0x00;
    // Bit 7:6 - COMnA1:0 = 00 - don't use Compare Output Mode Channel A
    // Bit 5:4 - COMnB1:0 = 00 - don't use Compare Output Mode Channel B
    // Bit 3:2 - COMnC1:0 = 00 - don't use Compare Output Mode Channel C
    // Bit 1:0 - WGMn1:0  = 00 - use normal counter mode, set TOP to 0xffff

  // §
  TCCR3B = 0x01; // a couple of things need Timer3 so just reenable it w/o an ICU
  TCCR4B = 0xC1; // 11000001
  TCCR5B = 0xC1;
    // Bit 7   - ICNCn    = 1   - input capture noise canceler or disable (0) or ENABLE (1) - PTACH stretched out to 6us (96 clock cycles), DSYNC asserted for 1/f_HSYNC = 9.96us (159 clock cycles)
    // Bit 6   - ICESn    = 1   - trigger Input Capture on falling edge (0) or RISING EDGE (1)
    // Bit 5   - reserved = 0
    // Bit 4:3 - WGMn3:2  = 00  - use normal counter mode, set TOP to 0xffff
    // Bit 2:0 - CSn2:0   = 001 - START CLOCK; no prescaling
  // set this register for both PTACH and DSYNC one right after another for minimal counter difference
  // enable these clocks AFTER inputting the correct offset into TCNTn to account for starting them at different times

  // §
  TCCR3C = 0x00;
  TCCR4C = 0x00; // 00000000 // default value
  TCCR5C = 0x00;
    // Bit 7   - FOCnA    = 0 - don't force output compare ch A
    // Bit 6   - FOCnB    = 0 - don't force output compare ch B
    // Bit 5   - FOCnC    = 0 - don't force output compare ch C
    // Bit 4:0 - reserved = 00000

  // §
  // ICRn
  // the register where timer/counter values are captured

  // §
  TIMSK3 = 0x00;
  TIMSK4 = 0x20; // 00100000
  TIMSK5 = 0x20;
    // Bit 7:6 - read only = 0
    // Bit 5   - ICIEn  = 1 - ENABLE (1) or disable (0) Input Capture interrupt
    // Bit 4   - read only = 0
    // Bit 3   - OCIEnC = 0 - don't use output compare C
    // Bit 2   - OCIEnB = 0 - don't use output compare B
    // Bit 1   - OCIEnA = 0 - don't use output compare A
    // Bit 0   - TOIEn  = 0 - don't interrupt on overflow

  // §
  // TIFRn
    // Bit 7:6 - read only
    // Bit 5   - ICFn - set when a capture event occurs on the ICPn pin
    // Bit 4   - read only
    // Bit 3   - OCFnC - ignore; not using output compare C interrupt
    // Bit 2   - OCFnB - ignore; not using output compare B interrupt
    // Bit 1   - OCFnA - ignore; not using output compare A interrupt
    // Bit 0   - TOVn  - ignore; not using overflow interrupt

  // §18.4
  // GTCCR - General Timer/Counter Control Register
    // Bit 7 - set to 1 activates Timer/Counter Synchronization mode; keeps all timer/counters halted
    //         set to 0 makes timer/counters start counting simultaneously
    // not sure if this works with f_CLK_I/O / 1 (clocked directly)
  
  
  
  // configure VSYNC interrupt
  EIMSK &= ~0x04; // disable INT2 (VSYNC) (see Note 1. of Table 15-1, pg. 110)
  EICRA |= 0x10; // set ISC21:0 to 11 to trigger INT2 (VSYNC) on rising edge (Sec. 15.2.1, pg. 110)
  EIFR  |= 0x04; // clear the INT2 interrupt flag before reenabling (Sec. 15.2.1, pg. 110)
  EIMSK |= 0x04; // enable INT2 (VSYNC)
  
  
  
  // enable all interrupts
  interrupts();
  
// END for Input Capture Units
  
  
  
// BEGIN before Input Capture Units were used
/*
  // interrupts

  EIMSK &= ~0x04; // disable INT2 (VSYNC) (see Note 1. of Table 15-1, pg. 110)
  EIMSK &= ~0x10; // disable INT4 (DSYNC) (see Note 1. of Table 15-3, pg. 111)
  EIMSK &= ~0x20; // disable INT5 (PTACH) (see Note 1. of Table 15-3, pg. 111)
  EICRA |= 0x10; // set ISC21:0 to 11 to trigger INT2 (VSYNC) on rising edge (Sec. 15.2.1, pg. 110)
  EICRA |= 0x20; //   (00 - trigger if low, 01 - any change, 10 - falling edge, 11 - rising edge)
  EICRB |= 0x01; // set ISC41:0 to 11 to trigger INT4 (DSYNC) on rising edge (Sec. 15.2.2, pg. 111)
  EICRB |= 0x02; //   (00 - trigger if low, 01 - any change, 10 - falling edge, 11 - rising edge)
  EICRB |= 0x04; // set ISC51:0 to 11 to trigger INT5 (PTACH) on rising edge (Sec. 15.2.2, pg. 111)
  EICRB |= 0x08; //   (00 - trigger if low, 01 - any change, 10 - falling edge, 11 - rising edge)
  EIFR  |= 0x04; // clear the INT2 interrupt flag before reenabling (Sec. 15.2.1, pg. 110)
  EIFR  |= 0x10; // clear the INT4 interrupt flag before reenabling
  EIFR  |= 0x20; // clear the INT5 interrupt flag before reenabling
  //EIMSK |= 0x04; // enable INT2 (VSYNC)
  EIMSK |= 0x10; // enable INT4 (DSYNC)
  EIMSK |= 0x20; // enable INT5 (PTACH)
  // enable all interrupts
  interrupts();
  //SREG |= 0x80; 

  // hardware counters

// Test: use only one clock to reduce ISR overhead
  TCCR3A = 0x00; // Timer/Counter 3 (DSYNC) Control Register A, p.154
  // bit 76, COM3A1:0 = 00 - not using output compare pins
  // bit 54, COM3B1:0 = 00 - not using output compare pins
  // bit 32, COM3C1:0 = 00 - not using output compare pins
  // bit 10, WGM31:0 = 00 - normal mode
  TCCR3B = 0x01; // Timer/Counter 3 (DSYNC) Control Register B, p.156
  // setting this starts the timer
  // bit 7, ICNC3 = 0 - set it if we use the Input Capture Pin in the future
  // bit 6, ICES3 = 0 - falling edge, 1 for rising edge, but we're not currently using this
  // bit 5, reserved = 0 - mandated by datasheet
  // bit 43, WGM33:2 = 00 - normal mode
  // bit 210, CS32:0 = 001 - use 001 for internal clock with no prescaling

  TCCR4A = 0x00; // (PHASE)
  TCCR4B = 0x01; // (PHASE)
  TCCR5A = 0x00; // (PTACH)
  TCCR5B = 0x01; // (PTACH)
*/
// END before Input Captuare Units were used
}