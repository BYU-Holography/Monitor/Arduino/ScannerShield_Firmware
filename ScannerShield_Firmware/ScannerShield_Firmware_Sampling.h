/////////////////////////////////////
//
// PRINTING / REPORTING
//
/////////////////////////////////////

//unsigned int DSYNC_period_cycles_remaining;
unsigned int DSYNC_period_cycles_surplus;
//unsigned int loop_clock_stop;

unsigned int loop_total_cycles;
unsigned int loop_computation_cycles;
unsigned int loop_computation_cycles_accumulator;
unsigned int loop_print_cycles;
unsigned int loop_print_cycles_accumulator;

/*
examples:

Human Print Mode:
DS      DSF     PT      PTF     FF      PeP     PeI     PhD     PhI     CMD
-54321  -54321  -54321  -54321  -54321  -54321  -54321  -54321  -54321  -54321
2nd line is updated and is all on one line
the terminal output must be wide enough to accommodate all enabled variables

Fast Print Mode:
xHHHHxHHHHxHHHHxHHHH... as compact / fast as possible

    ccc  c
 1  LTC  l  loop_total_cycles
 2  LCC  c  loop_computation_cycles
 3  DCB  b  DSYNC_cycles_budget
 4  MV   m  motor_value
 5  DP   d  DSYNC_period
 6  DPF  f  DSYNC_period_filtered
 7  TP   t  PTACH_period
 8  TPF  u  PTACH_period_filtered
 9  PV   p  PHASE_value
10  PVF  q  PHASE_value_filtered
11  PD   r  PHASE_delta
12  PeP  w  kp_period_term
13  PeI  x  ki_period_term
14  PhD  y  kd_phase_term
15  PhI  z  ki_phase_term
16  DIE  i  DSYNC_divider_inversion_event_count
         
"c" abbreviation can't be any capital hex digit (0-9,A-F); used in fast print mode
"ccc" abbreviation can be anything

note: increased SERIAL_TX_BUFFER_SIZE in HardwareSerial.h from 64 to 128 to accommodate transient prints

*/

typedef struct
{
  void* ptr;
  bool is_16b;
  bool is_signed;
  char c;
  char ccc[3];
  bool enabled;
  int  prevval; // 16-bit placeholder for all values; don't print 32-bit values; initialize to unlikely start value 0x1A57 ("last")
} PrintVar;


// number of characters we can print within one DSYNC period:
#define PRINT_CHARS_BUDGET 35
// PRINT_CHARS_BUDGET = DSYNC_period /( clock_frequency / baud_rate_bits_per_second * bits_per_char )
//                    =        10204 /(             16M /                      500k *             9 )
//                    = 35.4
#define PRINT_CHARS_MIN_THRESH 5

// TODO: automatically calculate the print budget

#define N_PRINT_VARS 17
unsigned char n_print_vars_enabled;
//                                                 ptr                           16b signed   c   |----ccc----|    ena  prevval
PrintVar print_vars[ N_PRINT_VARS ] = { { (void*) &loop_total_cycles,           true, false, 'l', 'L', 'T', 'C', false, 0x1A57 },
                                        { (void*) &loop_computation_cycles,     true, false, 'c', 'L', 'C', 'C', false, 0x1A57 },
                                        { (void*) &loop_print_cycles,           true, false, 'a', 'L', 'P', 'C', false, 0x1A57 },
                                        { (void*) &DSYNC_period_cycles_surplus, true,  true, 'b', 'D', 'C', 'S', false, 0x1A57 },
                                        { (void*) &motor_value,                 true, false, 'm', 'M', 'V', ' ', false, 0x1A57 },
                                        { (void*) &DSYNC_period,                true, false, 'd', 'D', 'P', ' ',  true, 0x1A57 }, // need to always print for timekeeping
                                        { (void*) &DSYNC_period_filtered,       true, false, 'f', 'D', 'P', 'F', false, 0x1A57 },
                                        { (void*) &PTACH_period,                true, false, 't', 'T', 'P', ' ', false, 0x1A57 },
                                        { (void*) &PTACH_period_filtered,       true, false, 'u', 'T', 'P', 'F', false, 0x1A57 },
                                        { (void*) &PHASE_value,                 true,  true, 'p', 'P', 'V', ' ',  true, 0x1A57 },
                                        { (void*) &PHASE_value_filtered,        true,  true, 'q', 'P', 'V', 'F', false, 0x1A57 },
                                        { (void*) &PHASE_delta,                 true,  true, 'r', 'P', 'D', ' ',  true, 0x1A57 },
                                        { (void*) &kp_period_term,              true,  true, 'w', 'P', 'e', 'P',  true, 0x1A57 },
                                        { (void*) &ki_period_term,              true,  true, 'x', 'P', 'e', 'I',  true, 0x1A57 },
                                        { (void*) &kd_phase_term,               true,  true, 'y', 'P', 'h', 'D', false, 0x1A57 },
                                        { (void*) &ki_phase_term,               true,  true, 'z', 'P', 'h', 'I',  true, 0x1A57 },
                                        { (void*) &DSYNC_divider_inversion_event_count, false, false, 'i', 'D', 'I', 'E', false, 0x1A57 } };

unsigned char PRINT_index_DSYNC_period = 5;


// at 500,000 baud, each serial bit takes 16M/500k = 32 clock cycles
// to transmit a byte, it takes 32 *( 8 bits/byte + 1 stop bit ) = 288 clock cycles
// nominally, we have DSYNC_period_filtered clock cycles (ie, 10204) before the DSYNC (and PTACH/PHASE) events occur
// with an example DSYNC period of 10204 clock cycles:
//   for transmission:
//     we can transmit 10204 / 288 = 35 characters (note that tabs = 1 character)
//   computationally, we have 10204 clock cycles to:
//     measure and react to DSYNC events
//     compute filtered variables
//     compute the control algorithm
//     write to the round-robin interrupt-driven serial transmission array
//     everything else
//
// you'll have to wait to print something if:
//   we've printed 35 characters this DSYNC period (the serial doesn't have the bandwidth for the amount
//                                                  of data we're trying to push through)
//   we've used up the 10204 clock cycles (the CPU is too slow and we'll miss DSYNC/PTACH/PHASE events)

//define PRINT_CYCLES_PER_RR_INSERTION 22
//define PRINT_INIT_CYCLES 345

bool          print_enabled = false; // enables/disables all printing
unsigned char print_var_i = 0; // print variable index
unsigned char print_var_i_start_loop = 0;
bool          print_mode_human = false; // print mode (human/fast)
bool          print_mode_human_init = true; // print labels at beginning
char          print_characters_budget = 0; // estimated number of characters left for this DSYNC period
unsigned int  print_cycles_budget = 0; // estimated number of clock cycles left for this DSYNC period


