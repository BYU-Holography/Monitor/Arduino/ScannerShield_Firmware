Input Capture Unit
Section 17.6, Page 140/435
Tue 24 Oct 2017 Drew Henrie


PORTS:


ICPn - Input Capture Pin


REGISTERS:


ICRn - counter value store location
ICNCn - enables noise canceler
ICESn - sets edge detector condition


CONFIGURATION:
Sec 17.11 - Register Description


ICFn (ICF4 in TIFR4, ICF5 in TIFR5) - Input Capture Flag, cleared by ISR or by writing a 1
ICIEn in TIMSKn (bit 5, not TICIEn) - set to enable interrupt
ACIC in ACSR - clear to select ICPn as source; clear ICFn after changing
ICNCn in TCCRnB - setting enables noise canceler - introduces additional four system clock cycles of delay; not affected by prescaler



// §
TCCRnA = 0x00; // 00000000 // default value
  // Bit 7:6 - COMnA1:0 = 00 - don't use Compare Output Mode Channel A
  // Bit 5:4 - COMnB1:0 = 00 - don't use Compare Output Mode Channel B
  // Bit 3:2 - COMnC1:0 = 00 - don't use Compare Output Mode Channel C
  // Bit 1:0 - WGMn1:0  = 00 - use normal counter mode, set TOP to 0xffff

// §
TCCRnB = 0xC1; // 11000001
  // Bit 7   - ICNCn    = 1 - input capture noise canceler or disable (0) or ENABLE (1) - PTACH stretched out to 6us (96 clock cycles), DSYNC asserted for 1/f_HSYNC = 9.96us (159 clock cycles)
  // Bit 6   - ICESn    = 1 - trigger Input Capture on falling edge (0) or RISING EDGE (1)
  // Bit 5   - reserved = 0
  // Bit 4:3 - WGMn3:2  = 00 - use normal counter mode, set TOP to 0xffff
  // Bit 2:0 - CSn2:0   = 001 - start clock; no prescaling
// set this register for both PTACH and DSYNC one right after another for minimal
// enable these clocks after inputting the correct offset into TCNTn to account for starting them at different times

// §
TCCRnC = 0x00; // 00000000 // default value
  // Bit 7   - FOCnA    = 0 - don't force output compare ch A
  // Bit 6   - FOCnB    = 0 - don't force output compare ch B
  // Bit 5   - FOCnC    = 0 - don't force output compare ch C
  // Bit 4:0 - reserved = 00000

// §
TCNTnH, TCNTnL = TCNTn
// set the timer/counter initial value before enabling the clock so that they appear to have both been started simultaneously

// §
// ICRn
// the register where timer/counter values are captured

// §
TIMSKn = 0x20; // 00100000
  // Bit 7:6 - read only = 0
  // Bit 5   - ICIEn  = 1 - ENABLE (1) or disable (0) Input Capture interrupt
  // Bit 4   - read only = 0
  // Bit 3   - OCIEnC = 0 - don't use output compare C
  // Bit 2   - OCIEnB = 0 - don't use output compare B
  // Bit 1   - OCIEnA = 0 - don't use output compare A
  // Bit 0   - TOIEn  = 0 - don't interrupt on overflow

// §
// TIFRn
  // Bit 7:6 - read only
  // Bit 5   - ICFn - set when a capture event occurs on the ICPn pin
  // Bit 4   - read only
  // Bit 3   - OCFnC - ignore; not using output compare C interrupt
  // Bit 2   - OCFnB - ignore; not using output compare B interrupt
  // Bit 1   - OCFnA - ignore; not using output compare A interrupt
  // Bit 0   - TOVn  - ignore; not using overflow interrupt

// §18.4
// GTCCR - General Timer/Counter Control Register
  // Bit 7 - set to 1 activates Timer/Counter Synchronization mode; keeps all timer/counters halted
  //         set to 0 makes timer/counters start counting simultaneously
  // not sure if this works with f_CLK_I/O / 1 (clocked directly)

  bit set
  bit clear
  
  "one extra cycle must be added when accessing lower (64 bytes of) I/O space"
  



OPERATION:


When a capture is triggered, TCNTn 16-bit coutnter value is written to ICRn and ICFn is set.

ICRn is done by first reading the low byte (ICRnL) and then the high byte (ICRnH); high byte copied into temporary register (TEMP)

An input capture can be triggered by software by controlling the port of the ICPn pin.

read ICRn as early as possible in ISR

don't change TOP value while using Input Capture unit

for measuring Duty Cycle:
can change edge sensing actively
after changing edge, ICFn must be cleared by software (writing a logical one to the I/O bit)













































