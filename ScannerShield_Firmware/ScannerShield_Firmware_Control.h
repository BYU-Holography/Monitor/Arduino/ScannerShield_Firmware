/////////////////////////////////////
//
// CONTROL ALGORITHM VARIABLES & FUNCTION
//
/////////////////////////////////////



// a flag for waiting until the motor has spun up from a standstill to 
//   a speed where we can measure the PTACH period without timer overflow 
         
unsigned int loop_clock_start;
unsigned int loop_clock_start_last;

unsigned int loop_clock_overflow_counter = 0;
        bool spinup_wait = false;

// these are made global so that computation and printing can be done in different contexts
int  period_error;
int  kp_period_term;
int  ki_period_term;
long ki_period_term_long_sum = 0L;
int  kd_phase_term;
int  ki_phase_term;
long ki_phase_term_long_sum = 0L;

bool ki_period_enable = false;

// enable/disable the controller
bool enable_period_control = false;

